import babel from 'rollup-plugin-babel' 

const config = {
  input: 'src/index.js',
  external: ['react', 'prop-types', 'graphql-tag', 'apollo-cache-inmemory', 'apollo-client', 'react-apollo', 'apollo-link-ws'],
  output: {
      format: 'umd',
      name: 'react-tibber-consumption',
      globals: {
          react: "React"
      }
  },
  plugins: [
    babel({
        exclude: "node_modules/**"
    }),
],
}
export default config
