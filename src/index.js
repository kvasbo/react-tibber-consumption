import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import gql from 'graphql-tag';
import { InMemoryCache } from 'apollo-cache-inmemory';
import ApolloClient from 'apollo-client';
import { Subscription, ApolloProvider } from 'react-apollo'
import { WebSocketLink } from 'apollo-link-ws';
import TibberRealtimeDisplay from './TibberRealtimeDisplay'

const ENDPOINT = `wss://api.tibber.com/v1-beta/gql/subscriptions`;

const CONSUMPTION_QUERY = gql`subscription liveConsumption($homeId: ID!) {
  liveMeasurement(homeId:$homeId)
    {
      timestamp
      power
      accumulatedConsumption
      accumulatedCost
      currency
      minPower
      averagePower
      maxPower
    }
  }`

class TibberRealtimeConsumptionWrapper extends PureComponent {
  constructor(props) {
    super(props);

    this.link = new WebSocketLink({
      uri: ENDPOINT,
      options: {
        reconnect: true,
        connectionParams: () => ({
          token: this.props.token,
        }),
      },
    });

   this.client = new ApolloClient({
    link: this.link,
    cache: new InMemoryCache()
  });
  }
  
  render() {
    return (
      <ApolloProvider
        client={this.client}
      >
        <div className="Realtime">
          <Subscription
            subscription={CONSUMPTION_QUERY}
            variables={{ homeId: this.props.homeId}}
            onSubscriptionData={data =>this.props.onData(data.subscriptionData.data.liveMeasurement)}
          >
          {result => {
            // don't display
            if (!this.props.display) {
              return null;
            }
            
            if (!result || !result.data || !result.data.liveMeasurement) {
              return (
                <div>Loading...</div>
              );
            }
            const data = result.data.liveMeasurement;
            const { children } = this.props;
            if (children) {
              const childrenWithProps = React.Children.map(children, child =>
                React.cloneElement(child, { ...data })
              );
              return <div>{childrenWithProps}</div>
            } else if (!this.props.display) {
              return null;
            } else {
              return (
                <TibberRealtimeDisplay { ...data } />
              );
            }
          }}
          </Subscription>
        </div>
      </ApolloProvider>
    );
  }
}

TibberRealtimeConsumptionWrapper.defaultProps = {
  onData: () => {},
  display: true,
}

TibberRealtimeConsumptionWrapper.propTypes = {
  homeId: PropTypes.string.isRequired,
  token: PropTypes.string.isRequired,
  onData: PropTypes.func,
  display: PropTypes.bool,
}

export default TibberRealtimeConsumptionWrapper;
