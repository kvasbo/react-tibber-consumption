import React, { PureComponent } from 'react';

class TibberRealtimeDisplay extends PureComponent {
  render() {
    return (
      <div className="TibberRealtimeDisplay">
        <h4>Realtime</h4>
        <div>Time {this.props.timestamp}</div>
        <div>Consumption {this.props.power} W</div>
        <h4>Power</h4>
        <div>Used today {this.props.accumulatedConsumption} kWh</div>
        <div>Average today {this.props.averagePower} kWh</div>
        <div>Max today {this.props.maxPower} kWh</div>
        <div>Min today {this.props.minPower} kWh</div>
        <h4>Cost</h4>
        <div>Used today {this.props.accumulatedCost} {this.props.currency}</div>
        <div>
          <h4>How to create your own display</h4>
          This is only an example! To show the data in your own way, please wrap your component in the <i>TibberRealtimeConsumptionWrapper</i> component. Any children will then have the following props: 

          <ul>
            <li>power</li>
            <li>timestamp</li>
            <li>accumulatedConsumption</li>
            <li>accumulatedCost</li>
            <li>averagePower</li>
            <li>currency</li>
            <li>maxPower</li>
            <li>minPower</li>
          </ul>

          Go wild!
          
        </div>
      </div>
    );
  }
}

export default TibberRealtimeDisplay;

